'''
Created on 5 avr. 2019

@author: ALI
'''
# -*- coding: utf-8 -*-
import re
from tkinter.messagebox import *
from tkinter.filedialog import *
from tkinter import *
from collections import OrderedDict

def fct1():
   
    fenetre = Tk()
    label = Label(fenetre, text="Ouverture du fichier en cours...")
    label.pack()
    filename = askopenfilename(title="Ouvrir votre document",filetypes=[('txt files','.txt'),('all files','.*')])
    fichier = open(filename,"r", encoding="utf8")
    fenetre.destroy()

    contenu = fichier.read()

    contenu_sans_ponct =re.findall(r"[\w']+", contenu.upper())
    return contenu_sans_ponct
def fcnt2(bi,contenu_sans_ponct):
    i=0
    list_bi=[]
    while i<len(contenu_sans_ponct)-bi:
        j=i
        l=[]
        while (j<i+bi):
            l.append(contenu_sans_ponct[j])
            j+=1
        list_bi.append(l)
        i=i+1
    return list_bi
#def affich(lst):     [print(j, i) for i, j in lst]

def listversDic(ls):
    wordDict = {}
    for word in ls:
        
        if (tuple(word) in wordDict):
            wordDict[tuple(word)] += 1
        else:
            key=tuple(word)
            value=1
            wordDict[key]=value
      
    return wordDict

def order(dic):# order la list
    return OrderedDict(sorted(dic.items(), key=lambda x: x[1],reverse=True))
def listocc(dic):
   
    ls=[]
    i=1   
    for key in dic :
        ls.append((key,dic[key],i))
        i+=1  
    return ls
def fonc(lst):
    i=2
    j=1
    ls=[lst[0]]
    del(lst[0])
    
    while (len(lst)!=0):
        d=lst[0]
        
        if(d[1]==ls[len(ls)-1][1]):
            ls.append((d[0],d[1],ls[j-1][2]))
            i+=1
           
        else:
            ls.append((lst[0][0],lst[0][1],i))
            i+=1
        del(lst[0])
        j+=1
       
    return ls
#POUR LES NGRAM LES LETTRE :
def ngrammot(mot,nb):
    n=[]
    if(nb>len(mot)):
        return n
    else:
        i=0
       
        while(i+nb<len(mot)+1):
            n.append(mot[i:i+nb])
            i+=1
    return n
       
def toutllesngram(lst,nb):
    d={}
    for mot in lst:
        for  s in ngrammot(mot,nb):
            if s in d:
                d[s]+=1
            else :
                d[s]=1
    return d
def fcnt5(lst,n):
    return zip(*[lst[i:] for i in range(n)])
    
   
d=fct1()
print ("--------------------------------------------------------")
#loi de Zipf 1 mot
print ("Partie 1")
b=fcnt2(1,d)

c=listversDic(b)
d=order(c)
ls= listocc(d)
a= fonc(ls)
print ("rang","\t occurance:","\tfrequence*rang :","\tle mot :")
for d in a:
    ch=''
    i=0
    while(i<len(d[0])):
        ch+=' '+d[0][i]
        i+=1
    
    print (d[2],"\t",d[1],"\t",d[1]*d[2],"\t",ch)  

print ("--------------------------------------------------------")
#loi de Zipf + ngram ; 2  mot selon le valeur , ici  j'ai utiliser 2
print ("Partie 2")
d=fct1()
b=fcnt2(3,d)

c=listversDic(b)
d=order(c)
ls= listocc(d)
a= fonc(ls)
print ("rang","\trang occurance :","\tfrequence*rang :","\tle mot :")
for d in a:
    ch=''
    i=0
    while(i<len(d[0])):
        ch+=' '+d[0][i]
        i+=1
    
    print (d[2],"\t",d[1],"\t",d[1]*d[2],"\t",ch)
print ("--------------------------------------------------------")

d=fct1()
print ("partie 3 I")
r=order(toutllesngram(d,1))
for s in r :
    print (s,r[s])
print ("--------------------------------------------------------")
d=fct1()
print ("partie 3 II")
r=order(toutllesngram(d,1))
for s in r :
    print (s,r[s])


