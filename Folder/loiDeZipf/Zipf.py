'''
Created on 5 avr. 2019

@author: ALI
'''
import codecs

from collections import OrderedDict
stop_words=frozenset(['us','too','a',',','.', 'about', 'above', 'after', 'again', 'against', 'all', 'am', 'an', 'and', 'any', 'are', "aren't", 'as', 'at', 'be', 'because', 'been', 'before',
            'being', 'below', 'between', 'both', 'but', 'by', "can't", 'cannot', 'could', "couldn't", 'did', "didn't", 'do', 'does', "doesn't", 'doing', "don't",
            'down', 'during', 'each', 'few', 'for', 'from', 'further', 'had', "hadn't", 'has', "hasn't", 'have', "haven't", 'having', 'he', "he'd", "he'll", "he's",
            'her', 'here', "here's", 'hers', 'herself', 'him', 'himself', 'his', 'how', "how's", 'i', "i'd", "i'll", "i'm", "i've", 'if', 'in', 'into', 'is', "isn't",
            'it', "it's", 'its', 'itself', "let's", 'me', 'more', 'most', "mustn't", 'my', 'myself', 'no', 'nor', 'not', 'of', 'off', 'on', 'once', 'only', 'or',
            'other', 'ought', 'our', 'ours', 'ourselves', 'out', 'over', 'own', 'same', "shan't", 'she', "she'd", "she'll", "she's", 'should', "shouldn't", 'so',
            'some', 'such', 'than', 'that', "that's", 'the', 'their', 'theirs', 'them', 'themselves', 'then', 'there', "there's", 'these', 'they', "they'd", "they'll",
            "they're", "they've", 'this', 'those', 'through', 'to', 'too', 'under', 'until', 'up', 'very', 'was', "wasn't", 'we', "we'd", "we'll", "we're", "we've",
            'were', "weren't", 'what', "what's",'when', "when's", 'where', "where's", 'which', 'while', 'who', "who's", 'whom', 'why', "why's", 'with', "won't",
            'would', "wouldn't", 'you', "you'd", "you'll", "you're", "you've", 'your', 'yours', 'yourself', 'yourselves'])

import  re
def getWords(filepath):#ouvrir fishier et recupere tout ses mots dans une list 
    with codecs.open(filepath,'r','utf-8') as f:
        words = []
        for line in f:
            for word in line.split():
                word=re.sub(r"[^a-zA-Z]+", ' ', word)
                word=word.lower()
                if word not in stop_words:
                    words.append(word)
    return words

def countWords(lines):#a partir  d'une list creer dictionnaire
    wordDict = {}
    for word in lines:
    
        if word in wordDict: wordDict[word] += 1
        else: wordDict[word] = 1
    return wordDict

def order(dic):#order la list
    return OrderedDict(sorted(dic.items(), key=lambda x: x[1],reverse=True))

def listocc(dic):
   
    ls=[]
    i=1   
    for key in dic :
        ls.append((key,dic[key],i))
        i+=1  
    return ls
def fonc(lst):
    i=2
    j=1
    ls=[lst[0]]
    del(lst[0])
    
    while (len(lst)!=0):
        d=lst[0]
        
        if(d[1]==ls[len(ls)-1][1]):
            ls.append((d[0],d[1],ls[j-1][2]))
            i+=1
           
        else:
            ls.append((lst[0][0],lst[0][1],i))
            i+=1
        del(lst[0])
        j+=1
       
    return ls


a= getWords("../../Tal/RI/dataSet/sre01.txt")
b=order(countWords(a))

ls= listocc(b)
a= fonc(ls)
for d in a:
    print (d[0],"\t",d[1],"\t",d[2],"\t",d[1]*d[2])
  

       

    
