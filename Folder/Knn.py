
import csv
filepath="./dataset/iris.csv"

import math
import operator

def loadDataset(filename,  trainingSet=[] ):
    with open(filename, 'rt', encoding="UTF8") as csvfile:
        lines = csv.reader(csvfile)
        dataset = list(lines)
        
        for x in range(1,len(dataset)-1):
            
            for y in range(4):
                dataset[x][y] = float(dataset[x][y])
                trainingSet.append(dataset[x])
          

def euclideanDistance(instance1, instance2, length):
    distance = 0
    for x in range(length):
        distance += pow((instance1[x] - instance2[x]), 2)
    return math.sqrt(distance)

def getNeighbors(trainingSet, testInstance, k):
    distances = []
    length = len(testInstance)-1
    for x in range(len(trainingSet)):
        dist = euclideanDistance(testInstance, trainingSet[x], length)
        distances.append((trainingSet[x], dist))
    distances.sort(key=operator.itemgetter(1))
    neighbors = []
    for x in range(k):
        neighbors.append(distances[x][0])
    return neighbors

def getResponse(neighbors):
    classVotes = {}
    for x in range(len(neighbors)):
        response = neighbors[x][-1]
        if response in classVotes:
            classVotes[response] += 1
        else:
            classVotes[response] = 1
    sortedVotes = sorted(classVotes.items(), key=operator.itemgetter(1), reverse=True)
    return sortedVotes[0][0]
    

    # prepare data
trainingSet=[]
testSet=[]
loadDataset(filepath,  trainingSet)
print ('Train set: ' + repr(len(trainingSet)))

k = 3
testSet=[134,6.3,2.8,5.1,"Iris-virginica"]
    
neighbors = getNeighbors(trainingSet, testSet, k)
result = getResponse(neighbors)
print('> predicted=' + repr(result) + ', actual=' + repr(testSet[-1]))
    