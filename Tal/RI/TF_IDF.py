from os import path,listdir
from os.path import isfile, join
import io
import math
from nltk.stem import PorterStemmer
import re


#import nltk
#from nltk.corpus import stopwords
 
hash_map = {}
file_name_map={}

# stop_words=stopwords.words('english')
stop_words=frozenset(['a',',','.', 'about', 'above', 'after', 'again', 'against', 'all', 'am', 'an', 'and', 'any', 'are', "aren't", 'as', 'at', 'be', 'because', 'been', 'before',
            'being', 'below', 'between', 'both', 'but', 'by', "can't", 'cannot', 'could', "couldn't", 'did', "didn't", 'do', 'does', "doesn't", 'doing', "don't",
            'down', 'during', 'each', 'few', 'for', 'from', 'further', 'had', "hadn't", 'has', "hasn't", 'have', "haven't", 'having', 'he', "he'd", "he'll", "he's",
            'her', 'here', "here's", 'hers', 'herself', 'him', 'himself', 'his', 'how', "how's", 'i', "i'd", "i'll", "i'm", "i've", 'if', 'in', 'into', 'is', "isn't",
            'it', "it's", 'its', 'itself', "let's", 'me', 'more', 'most', "mustn't", 'my', 'myself', 'no', 'nor', 'not', 'of', 'off', 'on', 'once', 'only', 'or',
            'other', 'ought', 'our', 'ours', 'ourselves', 'out', 'over', 'own', 'same', "shan't", 'she', "she'd", "she'll", "she's", 'should', "shouldn't", 'so',
            'some', 'such', 'than', 'that', "that's", 'the', 'their', 'theirs', 'them', 'themselves', 'then', 'there', "there's", 'these', 'they', "they'd", "they'll",
            "they're", "they've", 'this', 'those', 'through', 'to', 'too', 'under', 'until', 'up', 'very', 'was', "wasn't", 'we', "we'd", "we'll", "we're", "we've",
            'were', "weren't", 'what', "what's",'when', "when's", 'where', "where's", 'which', 'while', 'who', "who's", 'whom', 'why', "why's", 'with', "won't",
            'would', "wouldn't", 'you', "you'd", "you'll", "you're", "you've", 'your', 'yours', 'yourself', 'yourselves'])

mypath = "./dataSet/"
files = [ f for f in listdir(mypath) if (isfile(join(mypath,f)) and f!= path.basename(__file__)) ]
print ("les fichier sont :",files)
number_of_files=0

for dirs in files:
    number_of_files += 1
    file_name_map[dirs] = number_of_files
    
    file = io.open(mypath+dirs, 'r+b')
    mybytes = file.read()
    a = str(mybytes)
    b = a.lower()
    
    file.close()
    
    mainArray=[]
    for word in b.split(" "):
        #word =word.rstrip('?:!.,;#\'"1234567890|')
        word=re.sub(r"[^a-zA-Z]+", ' ', word)
        mainArray.append(word)   
    position_counter = 0
    for k in mainArray:
        #if k in stop_words or len(k) < 2 :
            #mainArray.remove(k)
            #pass
        #elif re.match("^\d+?", k):
            #mainArray.remove(k)

        
        #else:
        if k not in stop_words and len(k)>=2 :
            position_counter += 1
            k = PorterStemmer().stem(k)
            #k = my_porter.stem(k)
            if k not in hash_map.keys():
               
                hash_map[k] = [[dirs, 1, [position_counter]]]  #list contient , nombre de line ; p          
                
            
            else:
                
                if hash_map[k][-1][0] == dirs:
                    hash_map[k][-1][1] += 1
                    hash_map[k][-1][2].append(position_counter)
                    
                    
                else:
                    hash_map[k].append([dirs, 1, [position_counter]])

for key in sorted(hash_map):
    
    
    print (key,"\t \t\t\t",str(hash_map[key]))
    print ("idf*tf = ",(1+math.log((hash_map[key][0][1]),10))*math.log(len(files)/len(hash_map[key]),10))
    print("----------------")
    



